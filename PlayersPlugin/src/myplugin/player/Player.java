package myplugin.player;

public class Player {
	
	private String name;
	
	private int number;
	
	
	public Player(String name, int number){
		this.name = name;
		this.number = number;
	}


	@Override
	public String toString() {
		return "Player [name=" + name + ", number=" + number + "]";
	}


	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}


	public int getNumber() {
		// TODO Auto-generated method stub
		return number;
	}
	
}

package myplugin.player;

import java.util.ArrayList;

public class PlayerManager implements PlayerInterface {

	private ArrayList<Player> list = new ArrayList<Player>();

	private int num = 1;
	private int deleted, founded;

	@Override
	public void displayPlayers() {
		System.out.println("this is the list of the players");
		for (int i = 0; i < list.size(); i++) {
			System.out.println((i + 1) + "" + list.get(i));
		}

	}

	@Override
	public void addPlayer(String name, int num) {
		System.out.println("creating a real player");
		Player p = new Player(name, num);
		System.out.println("just created " + p);
		list.add(p);
		System.out.println("and has been inserted to list, now list has"
				+ list.size());
	}

	@Override
	public void removePlayer(String name, int num) {
		deleted = 0;
		for (Player p : list) {
			if (p.getName() == name && p.getNumber() == num)
				;
			list.remove(p);
			deleted++;
		}
		if (deleted == 0) {
			System.out.println(name + " was not removed");
		}
	}

	@Override
	public void searchPlayer(String name) {
		founded = 0;
		for (Player p : list) {
			if (p.getName() == name) {
				System.out.println("founded " + p);
			}

			if (founded == 0) {
				System.out.println(name + " was not found");
			}
		}

	}
}

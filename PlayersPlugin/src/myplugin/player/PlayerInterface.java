package myplugin.player;

public interface PlayerInterface {
	public void addPlayer(String name, int num);
	
	public void removePlayer (String name, int num);
	
	public void searchPlayer (String name);
	
	public void displayPlayers();
}

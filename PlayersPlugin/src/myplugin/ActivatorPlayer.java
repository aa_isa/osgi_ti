package myplugin;

import myplugin.player.PlayerInterface;
import myplugin.player.PlayerManager;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;


public class ActivatorPlayer implements BundleActivator {
	ServiceRegistration servicePlayer;
	public void start(BundleContext context) throws Exception {
		System.out.println("My Plugin Starting!!");
		PlayerInterface playerInterface = new PlayerManager();
		servicePlayer = context.registerService(PlayerInterface.class.getName(), playerInterface, null);
		
	}
	
	
	public void stop(BundleContext context) throws Exception {
		System.out.println("My Plugin Stopping!!");
		servicePlayer.unregister();
	}

}

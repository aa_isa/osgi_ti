package servletplugin;

import myplugin.player.PlayerInterface;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import servletplugin.servlet.Contract;
import servletplugin.servlet.HttpServiceTracker;

public class ActivatorServlet implements BundleActivator {

	private HttpServiceTracker serviceTracker;

	ServiceReference playerService;
	PlayerInterface playerInterface;

	public void start(BundleContext context) throws Exception {
		System.out.println("Starting Servlet!!");
		serviceTracker = new HttpServiceTracker(context);
		serviceTracker.open();

		playerService = context.getServiceReference(PlayerInterface.class.getName());
		playerInterface = (PlayerInterface) context.getService(playerService);
		
		Contract.playerInterface = this.playerInterface;

	}

	public void stop(BundleContext context) throws Exception {
		System.out.println("Killing Servlet!!");
		serviceTracker.close();
		serviceTracker = null;
	}

}

package servletplugin.servlet;

import java.io.IOException;

import javax.naming.Context;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import servletplugin.ActivatorServlet;
import myplugin.player.PlayerInterface;

public class SimpleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	//committ
	//secondo commento
	//terzo commento
	//4
	PlayerInterface playerInterface;
	String type = "";
	String name, number;
	int num;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/plain");
		playerInterface = Contract.playerInterface;
		type = req.getHeader("type");

		
		
		if(type == null){
			resp.getWriter().write("inserisci parametri");
		}
		
		switch (type) {
		case "add":
			name = req.getHeader("name");
			number = (req.getHeader("number"));
			num = Integer.parseInt(number);
			playerInterface.addPlayer(name, num);
			resp.getWriter().write(
					"il giocatore " + name + " ha il numero " + number);
			break;
		case "remove":
			name = req.getHeader("name");
			number = (req.getHeader("number"));
			num = Integer.parseInt(number);
			playerInterface.removePlayer(name, num);

			break;
		case "search":
			name = req.getHeader("name");
			playerInterface.searchPlayer(name);

			break;
		case "print":
			playerInterface.displayPlayers();
			break;
		default:
			resp.getWriter().write("la tua richiesta non e' disponibile");
			break;

		}

	}
}